export default [
  {
      path: '/organizations/',
      component: 'organizations'
  },
  {
    path: '/organizations/:id/',
    component: 'organizations'
  },
  {
    path: '/create/:type/',
    component: 'create'
  },
  {
      path: '/form/',
      component: require('./pages/form.vue')
  },
  {
      path: '/dynamic-route/blog/:blogId/post/:postId/',
      component: require('./pages/dynamic-route.vue')
  }
]
