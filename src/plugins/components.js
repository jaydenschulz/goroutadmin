import Vue from 'vue'
import comp from './../components'
import {Avatar} from 'vue-avatar'
import {OfflineIndicator,VueOnline} from 'vue-online'



let components = {}

components.install = function install(Vue,params){

  let vuetags = {
    'gr-org-single': comp.organizationSingle,
    'gr-org-list': comp.organizationList,
    'gr-org-create':comp.organizationCreate,
    'gr-floating-action-buttons': comp.floatingActionButton,
    'gr-right-panel': comp.rightPanel,
    'gr-left-panel': comp.leftPanel,
    'gr-avatar': Avatar,
    'gr-offline-indicator': OfflineIndicator,
    'gr-online-vue':VueOnline
  }

debugger
  for(let [name,component] of new Map(Object.entries(vuetags))){
    Vue.component(name,component)
  }
}


export default {
  components
}
