/**
 * Created by jayden on 1/13/17.
 */

import {Plugins} from './../class/Plugins'
import {components as cmp} from './components'
import {VueFire} from 'vuefire'
import Framework7Vue from 'framework7-vue'

let pluginsInstance = new Plugins()

pluginsInstance.add(cmp)
pluginsInstance.add(VueFire)
pluginsInstance.add(Framework7Vue)
