/**
 * Created by jayden on 1/13/17.
 */

import {loginWithGoogle} from './../helper/firebaseAdapter'

export default {
  methods:{
    loginWithGoogle
  }
}
