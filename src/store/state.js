/**
 * Created by jayden on 1/13/17.
 */

import {database} from './../helper/firebaseAdapter'


export function getState(Vue){
  return new Promise(function(resolve){
    debugger
    database.ref().once('value',function(snapshot){

      let state = {
        organizations:{x:true},
        users:{},
        onlineUsers:{}
      }

      debugger
      state.organizations = snapshot.child('organizations').val()
      state.users = snapshot.child('users').val()
      state.onlineUsers = snapshot.child('onlineUsers').val()
      resolve(Vue, state)
    })
  })
}
