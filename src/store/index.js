/**
 * Created by jayden on 1/13/17.
 */

import Freezer from 'freezer-js'
import lodash from 'lodash'

let assign = lodash.assign
export let VueFreeze = function install(Vue,opt){

debugger
  // Check the Freezer~
  if( typeof Freezer == 'undefined' ) return console.warn('[Vue Freeze]: You Must Install Freezer.js firs!')

  var plugin = this

  // Plugin Store
  if(typeof opt  == 'undefined') {
    return console.warn('[Vue Freeze]: Please Specify the store! Via Vue.use Options!')
  }
  else {
    // live update?
    var live = opt.live ? opt.live : true

    // make our store!
    var store =  new Freezer(opt.state,{live:live})

    // binding the action
    opt.action(store)
  }

  // Make a data become mutable
  var mutable = (data) => JSON.parse(JSON.stringify(data))

  // Make a mixin
  plugin.mixin = {}

  // To make the state become watchable!
  plugin.mixin.data = function(){
    return{
      state: {}
    }
  }

  // Mixin Methods For vm instance
  plugin.mixin.methods = {
    // set the vm state to new state
    updateState(old,val){
      let newState = assign({},this.state,store.get().toJS())
      this.$set('state',newState)
    }
  }
  debugger

  // Mixin Created Event
  plugin.mixin.created = function() {
    var me = this

    // set global state as internal state
    me.$set('state',mutable(store.get()))

    // Make Methods
    Vue.util.defineReactive(this,'$store',store)
    Vue.util.defineReactive(this,'$action', function(eventName,arg){
      // trigger freezer event with pass the old value at the end
      store.trigger(eventName,arg,me.state)
    })

    // When Store Updated~
    me.$store.on('update', function (val) {
      // Update the state
      me.updateState(me.state,val)
    })
  }

  // Merge mixin to VM via vue options
  Vue.options = Vue.util.mergeOptions(Vue.options, plugin.mixin)

} // install()
