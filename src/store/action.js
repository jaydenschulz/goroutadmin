/**
 * Created by jayden on 1/13/17.
 */

import {database} from './../helper/firebaseAdapter'

/**
 *
 * @param store {VueFreeze.mixin.}
 */
export function actions(store){

  let init = true

  database.ref().on('value',function(snapshot){
    if(init){
      init=false
    }else{
      let organizations = snapshot.child('organizations').val()
      let users = snapshot.child('users').val()
      let onlineUsers = snapshot.child('onlineUsers').val()
      store.get().set({organizations, users, onlineUsers})
    }
  })

  store.on('org:add',function(org){
    database.ref('organizations').push().set(org)
  })
}
