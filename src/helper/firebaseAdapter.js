/**
 * Created by jayden on 1/9/17.
 * @author Jayden Schulz <jayden@gorout.com>
 */

//Import the firebae object
import firebase from 'firebase'

//Firebase config
let firebaseConfig = {
  apiKey: "AIzaSyCOuCxQ8cjLtV6Vi3EGV-5rTbpHTnEUpeo",
  authDomain: "admin-858f6.firebaseapp.com",
  databaseURL: "https://admin-858f6.firebaseio.com",
  storageBucket: "admin-858f6.appspot.com",
  messagingSenderId: "783436686465"
}
//Initialize the firebase object
firebase.initializeApp(firebaseConfig)

/**
 * Function to set the onAuthChange function which will trigger auth and login
 * @param onAuthChangeCallback
 */
export function onAuthChanged(onAuthChangeCallback){
  //Set the onAuthStateChanged Listener
  firebase.auth().onAuthStateChanged(function(user){
    if(user){
      onAuthChangeCallback(true,user)
    }else{
      onAuthChangeCallback(false,{})
    }
  },function(error){
    throw error
  })
}

export const loginWithGoogle = function loginWithGoogle(){
  firebase.auth().signInWithRedirect(new firebase.auth.GoogleAuthProvider()).catch(function(error){
    throw error
  })
}

/**
 * Firebase database instance
 * @type {!firebase.database.Database}
 */
export const database = firebase.database()

export default {
  onAuthChanged,
  database
}
