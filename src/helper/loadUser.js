// fid-umd {"name":"loadUser"}
(function (name, root, factory) {
    function isObject(x) { return typeof x === "object"; }
    if (isObject(module) && isObject(module.exports)) {
        module.exports = factory();
    } else if (isObject(exports)) {
        exports[name] = factory();
    } else if (isObject(root.define) && root.define.amd) {
        root.define(name, [], factory);
    } else if (isObject(root.modulejs)) {
        root.modulejs.define(name, factory);
    } else if (isObject(root.YUI)) {
        root.YUI.add(name, function (Y) { Y[name] = factory(); });
    } else {
        root[name] = factory();
    }
}("loadUser", this, function () {
    // fid-umd end
/**
 * Created by jayden on 1/13/17.
 */
function loadUser(user){
  console.log(user)
}

    // fid-umd post
}));
// fid-umd post-end
