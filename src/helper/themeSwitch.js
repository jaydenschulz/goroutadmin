/**
 * Created by jayden on 1/7/17.
 */


export default function deviceTheme(device) {
  if (device.android) {
      require("./../../static/framework7/css/framework7.material.min.css")
      require("./../../static/framework7/css/framework7.material.colors.min.css")
  }
  else {
    require("./../../static/framework7/css/framework7.ios.min.css")
    require("./../../static/framework7/css/framework7.ios.colors.min.css")
  }
}
