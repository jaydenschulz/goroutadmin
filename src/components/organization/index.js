/**
 * Created by jayden on 1/13/17.
 */


import organizationCreate from './create.vue'

import organizationSingle from './single.vue'

import organizationList from './list.vue'

export default {
  organizationList,
  organizationCreate,
  organizationSingle
}
