/**
 * Created by jayden on 1/13/17.
 */

import floatingActionButton from './floating-action-button.vue'

import leftPanel from './left-panel.vue'

import rightPanel from './right-panel.vue'

import org from './organization'


export default {
  floatingActionButton,
  leftPanel,
  rightPanel,
  organizationSingle:org.organizationSingle,
  organizationCreate:org.organizationCreate,
  organizationList:org.organizationList
}
