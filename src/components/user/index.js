/**
 * Created by jayden on 1/13/17.
 */

export userCreate from './create.vue'

export userSingle from './single.vue'

export userList from './list.vue'
