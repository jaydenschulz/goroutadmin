// Import Vue
import Vue from 'vue'
//Import Raven
import Raven from 'raven-js'
//Import Raven Vue plugin
import RavenVue from 'raven-js/plugins/vue'
//Initialize the raven error logger with our vue instance
Raven.config('http://815a1b135521496d9013f2abc1c0ff04@107.178.219.41/2').addPlugin(RavenVue, Vue).install()
//Import firebase helper and auth check
import {onAuthChanged,database, loginWithGoogle} from 'helper/firebaseAdapter'
import {VueFreeze} from './store'
// Import F7
import 'framework7'
//Import isMobile check
import 'helper/isMobile'
//Import device detect
import {device} from './helper/DeviceDetect'
//Set the template7 vars
window.Template7.global = {
  android: device.android,
  ios: device.ios
}
//Import dynamic theme resolver
import themeSwitch from './helper/themeSwitch'
//Resolve the theme
themeSwitch(device)
// Import App Custom Styles
import AppStyles from './css/app.css'
import AppStyles2 from './css/framework7-icons.css'
// Import Routes
import Routes from './routes.js'
// Import App Component
import App from './App'
import {getState} from './store/state'
import {actions} from './store/action'
import 'pages'
getState(Vue).then(function(Vue, state){
  debugger
  Vue.use(VueFreeze,{state:state,action:actions,live:false})

  // Init App
  window['vm'] = new Vue({
    el: '#app',
    template: '<app :data="{organizations,onlineUsers,users}"/>',
    // Init Framework7 by passing parameters here
    framework7: {
      root: '#app',
      material: (!device.ios),
      routes: Routes,
    },
    // Register App Component
    components: {
      app: App
    },
    methods:{
      loginGoogle:function(){
        loginWithGoogle();
      }
    },
    firebase:{
      organizations: {
        source: database.ref('organizations'),
        asObject:true
      },
      onlineUsers: database.ref('onlineUsers'),
      users: database.ref('users')
    }
  });
})



//import Authentication from 'mixins/Authentication'


onAuthChanged(function(loggedIn, user){
  loggedIn ? loadUser(user) : vm.$f7.loginScreen()
})

function loadUser(user){
  console.log(user)
}
