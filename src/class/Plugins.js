/**
 * Created by jayde on 1/14/2017.
 */

/**
 * class to easily add all plugins at once
 */
export class Plugins {
  constructor(){
    this.list = new Set()
  }
  add(plugin, opts = {}){
    this.list.add(plugin)
  }
  install(Vue,opts){
    let plugins = this.list.entries()

    for([plugin,opts] of plugins){
      Vue.use(plugin,opts)
    }
  }
}
