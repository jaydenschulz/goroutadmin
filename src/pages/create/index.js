/**
 * Created by jayden on 1/11/17.
 */
import Vue from 'vue'

import organization from './organization.vue'
import user from './user.vue'

let templates = {
  user,
  organization
}
Vue.component('create',{
  template:'<gr-create-org v-if="this.$route.params.type === \'organization\'"></gr-create-org><gr-create-user v-else-if="this.$route.params.type === \'user\'"></gr-create-user>',
  components:{
    'gr-create-org': organization,
    'gr-create-user': user
  }
})
