/**
 * Created by jayden on 1/11/17.
 */
import Vue from 'vue'

import list from './list.vue'

import single from './single.vue'

import {database} from 'helper/firebaseAdapter'

Vue.component('organizations',{
  template: '<gr-org-list></gr-org-list>',
  firebase:{
    organizations: database.ref('organizations'),
    orgObj: {
      source: database.ref('organizations'),
      asObject:true
    }
  },
  components:{
    'gr-org-list': list,
    'gr-org-details': single
  },
  methods:{
    addOrganization:function(){
      debugger
    }
  }
})
