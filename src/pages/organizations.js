/**
 * Created by jayden on 1/13/17.
 */

import Vue from 'vue'

Vue.component('organizations',{
  template:'<gr-org-list v-if="!$route.params.id"></gr-org-list>'
})
