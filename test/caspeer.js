/*==============================================================================*/
/* Casper generated Thu Jan 12 2017 14:45:53 GMT-0600 (Central Standard Time) */
/*==============================================================================*/
var casper = require('casper')
function takesnap(){
  var d = new Date()
  var snap = d.toISOString() + ".png"
  casper.capture(snap, {
    top: 0,
    left: 0,
    width: viewport.width,
    height: viewport.height
  });
}
var x = require('casper').selectXPath;
casper.options.viewportSize = {width: 412, height: 732};
casper.on('page.error', function(msg, trace) {
  takesnap()
  this.echo('Error: ' + msg, 'ERROR');
  for(var i=0; i<trace.length; i++) {
    var step = trace[i];
    this.echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR');
  }
});
casper.on('success',takesnap)
casper.test.begin('Resurrectio test', function(test) {
  casper.start('http://localhost:3000/');
  casper.waitForSelector(x("//a[normalize-space(text())='Organizations']"),
    function success() {
      test.assertExists(x("//a[normalize-space(text())='Organizations']"));
      this.click(x("//a[normalize-space(text())='Organizations']"));
    },
    function fail() {
      test.assertExists(x("//a[normalize-space(text())='Organizations']"));
    });
  casper.waitForSelector(x("//a[normalize-space(text())='GoRout Demo']"),
    function success() {
      test.assertExists(x("//a[normalize-space(text())='GoRout Demo']"));
      this.click(x("//a[normalize-space(text())='GoRout Demo']"));
    },
    function fail() {
      test.assertExists(x("//a[normalize-space(text())='GoRout Demo']"));
    });
  casper.waitForSelector(".item-link.active-state .item-inner",
    function success() {
      test.assertExists(".item-link.active-state .item-inner");
      this.click(".item-link.active-state .item-inner");
    },
    function fail() {
      test.assertExists(".item-link.active-state .item-inner");
    });
  casper.waitForSelector(".item-link.active-state .item-inner",
    function success() {
      test.assertExists(".item-link.active-state .item-inner");
      this.click(".item-link.active-state .item-inner");
    },
    function fail() {
      test.assertExists(".item-link.active-state .item-inner");
    });
  casper.waitForSelector(".back.icon-only.link.icon-only.active-state .icon.icon-back",
    function success() {
      test.assertExists(".back.icon-only.link.icon-only.active-state .icon.icon-back");
      this.click(".back.icon-only.link.icon-only.active-state .icon.icon-back");
    },
    function fail() {
      test.assertExists(".back.icon-only.link.icon-only.active-state .icon.icon-back");
    });
  casper.waitForSelector(".icon.icon-back",
    function success() {
      test.assertExists(".icon.icon-back");
      this.click(".icon.icon-back");
    },
    function fail() {
      test.assertExists(".icon.icon-back");
    });
  casper.waitForSelector(".icon.icon-bars",
    function success() {
      test.assertExists(".icon.icon-bars");
      this.click(".icon.icon-bars");
    },
    function fail() {
      test.assertExists(".icon.icon-bars");
    });
  casper.waitForSelector(".panel-overlay.active-state",
    function success() {
      test.assertExists(".panel-overlay.active-state");
      this.click(".panel-overlay.active-state");
    },
    function fail() {
      test.assertExists(".panel-overlay.active-state");
    });
  casper.waitForSelector(".icon.icon-close",
    function success() {
      test.assertExists(".icon.icon-close");
      this.click(".icon.icon-close");
    },
    function fail() {
      test.assertExists(".icon.icon-close");
    });
  casper.waitForSelector(".color-purple.active-state .icon.material-icons",
    function success() {
      test.assertExists(".color-purple.active-state .icon.material-icons");
      this.click(".color-purple.active-state .icon.material-icons");
    },
    function fail() {
      test.assertExists(".color-purple.active-state .icon.material-icons");
    });
  casper.waitForSelector(x("//a[normalize-space(text())='Cancel']"),
    function success() {
      test.assertExists(x("//a[normalize-space(text())='Cancel']"));
      this.click(x("//a[normalize-space(text())='Cancel']"));
    },
    function fail() {
      test.assertExists(x("//a[normalize-space(text())='Cancel']"));
    });
  casper.waitForSelector(".color-green.active-state .icon.material-icons",
    function success() {
      test.assertExists(".color-green.active-state .icon.material-icons");
      this.click(".color-green.active-state .icon.material-icons");
    },
    function fail() {
      test.assertExists(".color-green.active-state .icon.material-icons");
    });
  casper.waitForSelector(x("//a[normalize-space(text())='Cancel']"),
    function success() {
      test.assertExists(x("//a[normalize-space(text())='Cancel']"));
      this.click(x("//a[normalize-space(text())='Cancel']"));
    },
    function fail() {
      test.assertExists(x("//a[normalize-space(text())='Cancel']"));
    });

  casper.run(function() {test.done();});
});
